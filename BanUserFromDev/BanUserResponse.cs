﻿using Newtonsoft.Json;

namespace VaalBot.BanUserFromDev
{
    public class BanUserResponse
    {
        [JsonProperty(PropertyName = "data")]
        public BannedUser[] Data { get; protected set; }
    }
}
