﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

/// <summary>
/// See tutorial: https://docs.microsoft.com/en-us/aspnet/core/tutorials/signalr?view=aspnetcore-6.0&tabs=visual-studio
/// </summary>
namespace VaalBot.Hubs
{
    public class VaalHub : Hub
    {
        public const string VaalGroupId = "VAAL_GROUP";
        public async Task JoinVaalGroup()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, VaalGroupId);
        }

        //public async Task SendVaalResult(string user, int result)
        //{
        //    await Clients.All.SendAsync("ReceiveVaalResult", user, result);
        //}
    }
}