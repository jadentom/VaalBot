﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaalBot.ChatBot;

namespace VaalBot.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RewardsController : ControllerBase
    {
        [HttpGet]
        public ActionResult Details(string broadcaster)
        {
            if (!broadcaster.All(char.IsLetterOrDigit))
            {
                return BadRequest("Broadcaster must be alphanumeric.");
            }
            return Content(new RewardLogger(broadcaster).GetAllText());
        }
    }
}
