﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VaalBot.ChatBot
{
    public class RewardLogger
    {
        string profilePath;
        static readonly object logLock = new object();

        public RewardLogger(string channel)
        {
            var rewardLogPath = $"RewardLog_{channel.ToLower()}.csv";
            profilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), rewardLogPath);
            lock (logLock)
            {
                if (!File.Exists(profilePath))
                {
                    var lineToWrite = "User, Time, Source, Result, Notes";
                    lineToWrite += Environment.NewLine;
                    File.WriteAllLines(profilePath, new string[] { lineToWrite });
                }
            }
        }

        public void Log(string user, string source, int result = -1, string notes = "")
        {
            var lineToWrite = $"{user}, {DateTime.UtcNow}, {source}, {result}, {notes}";
            lineToWrite += Environment.NewLine;
            lock (logLock)
            {
                File.AppendAllLines(profilePath, new string[] { lineToWrite });
            }
        }

        public string GetAllText()
        {
            lock (logLock)
            {
                return File.ReadAllText(profilePath);
            }
        }
    }
}
