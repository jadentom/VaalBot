﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VaalBot.ChatBot
{
    public static class BotCredentialLoader
    {
        const string credentialsDefault = "BotCred.txt";
        public static (string username, string botClientId, string botAccessToken, string channel, string streamerClientId, string streamerAccessToken) LoadCredentials(string credentialsFile = credentialsDefault)
        {
            var profilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            var credentialsPath = Path.Combine(profilePath, credentialsFile);
            if (!File.Exists(credentialsPath))
            {
                throw new FileNotFoundException($"NO CREDENTIALS FILE AT {credentialsPath}");
            }
            var credentialsContents = File.ReadAllLines(credentialsPath);
            if (credentialsContents.Length < 6)
            {
                throw new InvalidOperationException($"CREDENTIALS FILE AT {credentialsPath} HAS LESS THAN 6 LINES");
            }
            return (credentialsContents[0].Trim(), credentialsContents[1].Trim(), credentialsContents[2].Trim(), credentialsContents[3].Trim(), credentialsContents[4].Trim(), credentialsContents[5].Trim());
        }
    }
}
