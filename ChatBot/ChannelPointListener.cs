﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.PubSub;
using TwitchLib.PubSub.Events;

namespace VaalBot.ChatBot
{
    public class ChannelPointListener
    {
        const string ChannelPointSource = "ChannelPoints";
        TwitchPubSub twitchPubSub;
        string channelId;
        string oAuthToken;
        Action<string, string> sendVaalMessage;

        public ChannelPointListener(string channelId, string oAuthToken, Action<string, string> sendVaalMessage)
        {
            this.oAuthToken = oAuthToken;
            this.sendVaalMessage = sendVaalMessage;
            this.channelId = channelId;

            twitchPubSub = new TwitchPubSub();
            twitchPubSub.OnPubSubServiceConnected += TwitchPubSub_OnPubSubServiceConnected;
            twitchPubSub.OnListenResponse += TwitchPubSub_OnListenResponse;
            // Debug: Not working
            twitchPubSub.OnChannelPointsRewardRedeemed += TwitchPubSub_OnChannelPointsRewardRedeemed;

            // Obsolete but the above is borked
            twitchPubSub.OnRewardRedeemed += TwitchPubSub_OnRewardRedeemed;
            twitchPubSub.Connect();
        }

        private void TwitchPubSub_OnPubSubServiceConnected(object? sender, EventArgs e)
        {
            twitchPubSub.ListenToChannelPoints(channelId);

            // Obsolete but the above is borked
            // twitchPubSub.ListenToRewards(channelId);
            twitchPubSub.SendTopics(oAuthToken);
        }

        private void TwitchPubSub_OnListenResponse(object? sender, OnListenResponseArgs e)
        {
            if (!e.Successful)
            {
                throw new Exception($"Failed to listen! Response: {e.Topic}");
            }
            else
            {
                Console.WriteLine($"Listening to {e.Topic}");
            }
        }

        private void TwitchPubSub_OnRewardRedeemed(object? sender, OnRewardRedeemedArgs e)
        {
            if (e.RewardTitle.Contains("vaal", StringComparison.OrdinalIgnoreCase))
            {
                sendVaalMessage(e.DisplayName, ChannelPointSource);
            }
        }

        private void TwitchPubSub_OnChannelPointsRewardRedeemed(object? sender, OnChannelPointsRewardRedeemedArgs e)
        {
            if (e.RewardRedeemed.Redemption.Reward.Title.Contains("vaal", StringComparison.OrdinalIgnoreCase))
            {
                sendVaalMessage(e.RewardRedeemed.Redemption.User.DisplayName, ChannelPointSource);
            }
        }
    }
}
