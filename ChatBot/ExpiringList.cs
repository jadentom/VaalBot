﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VaalBot.ChatBot
{
    public class ExpiringList
    {
        TimeSpan timeout;
        IDictionary<string, DateTime> internalDict = new Dictionary<string, DateTime>(StringComparer.OrdinalIgnoreCase);

        public ExpiringList(TimeSpan timeout)
        {
            this.timeout = timeout;
        }

        public void Add(string item)
        {
            var expiryTime = DateTime.UtcNow + timeout;
            lock (internalDict)
            {
                Cleanup();
                internalDict[item] = expiryTime;
            }
        }

        public bool Contains(string item)
        {
            lock (internalDict)
            {
                Cleanup();
                return internalDict.ContainsKey(item);
            }
        }

        public bool Remove(string item)
        {
            lock (internalDict)
            {
                Cleanup();
                return internalDict.Remove(item);
            }
        }

        void Cleanup()
        {
            var utcNow = DateTime.UtcNow;
            lock (internalDict)
            {
                var keysToRemove = new List<string>();
                foreach (var (key, value) in internalDict)
                {
                    if (utcNow > value)
                    {
                        keysToRemove.Add(key);
                    }
                }
                foreach (var key in keysToRemove)
                {
                    internalDict.Remove(key);
                }
            }
        }
    }
}
