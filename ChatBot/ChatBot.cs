﻿using Microsoft.AspNetCore.SignalR;
using VaalBot.Hubs;
using VaalBot.BanUserFromDev;
using System;
using System.Linq;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using System.Threading;

namespace VaalBot.ChatBot
{
    public class ChatBot
    {
        const string OverrideSource = "Override";
        const string PassedSource = "Passed";
        ChannelPointListener channelPointListener;
        TwitchClient client;
        string channel;
        string channelId;
        string botUserId;
        bool verbose;
        DateTime lastRoll = DateTime.MinValue;
        IHubContext<VaalHub> context;
        RewardLogger logger;
        TwitchLib.Api.TwitchAPI botApi;
        TwitchLib.Api.TwitchAPI streamerApi;
        ModerationWithBanUser moderationWithBanUser;

        ExpiringList vaalListWilling = new ExpiringList(TimeSpan.FromMinutes(1));
        ExpiringList vaalListPass = new ExpiringList(TimeSpan.FromMinutes(1));
        ExpiringList vaalListGift = new ExpiringList(TimeSpan.FromMinutes(1));
        ExpiringList vaalListTimeout = new ExpiringList(TimeSpan.FromMinutes(1));

        public ChatBot(IHubContext<VaalHub> context, bool verbose = false)
        {
            this.context = context;
            this.verbose = verbose;
            var (username, botClientId, botAccessToken, channel, streamerClientId, streamerAccessToken) = BotCredentialLoader.LoadCredentials();
            ConnectionCredentials credentials = new ConnectionCredentials(username, botAccessToken);
            this.channel = channel;

            client = new TwitchClient();
            client.Initialize(credentials, channel);

            client.OnLog += Client_OnLog;
            client.OnConnected += OnConnected;
            client.OnMessageReceived += OnMessageReceived;
            client.OnWhisperReceived += OnWhisperReceived;

            client.Connect();

            botApi = new TwitchLib.Api.TwitchAPI();
            botApi.Settings.ClientId = botClientId;
            botApi.Settings.AccessToken = botAccessToken;
            moderationWithBanUser = ModerationWithBanUser.Get(botClientId, botAccessToken);
            streamerApi = new TwitchLib.Api.TwitchAPI();
            streamerApi.Settings.ClientId = streamerClientId;
            streamerApi.Settings.AccessToken = streamerAccessToken;

            var tempBotUserId = GetUser(username)?.Id;
            if (tempBotUserId is null)
            {
                throw new InvalidOperationException($"COULD NOT GET ID FOR {username}");
            }
            botUserId = tempBotUserId;
            var tempChannelId = GetUser(channel)?.Id;
            if (tempChannelId is null)
            {
                throw new InvalidOperationException($"COULD NOT GET ID FOR {channel}");
            }
            channelId = tempChannelId;

            channelPointListener = new ChannelPointListener(channelId, streamerAccessToken, SendVaalMessage);
            logger = new RewardLogger(channel);
        }

        private string HandleVaalResult(int result, string userName, string userId)
        {
            string Timeout(TimeSpan timeoutLength, string friendlyName)
            {
                try
                {
                    var banReason = $"You rolled a {result} and were sacrificed!";
                    var banUserRequest = new BanUserRequest();
                    banUserRequest.UserId = userId;
                    banUserRequest.Duration = (int?)timeoutLength.TotalSeconds;
                    banUserRequest.Reason = banReason;
                    var banResult = moderationWithBanUser.BanUserAsync(channelId, botUserId, banUserRequest).Result;
                    // client.SendMessage(channel, $"/timeout {userName} {(int?)timeoutLength.TotalSeconds} {banReason}"); This doesn't work.
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException($"Could not time out user ${userName}, the bot may not have permissions or the user may be a mod.", e);
                }
                return $"They have been sacrificed for {friendlyName}!";
            }

            // I remember only 1 - 1h timeout, 2 - 24 hour timeout, 3 - 3day timeout, 4 - pass the vaal on, 5- gift sub 6 - harvest someone for 1h
            switch (result)
            {
                case 1:
                    return Timeout(TimeSpan.FromHours(1), "an hour");
                case 2:
                    return Timeout(TimeSpan.FromDays(1), "a day");
                case 3:
                    return Timeout(TimeSpan.FromDays(3), "THREE DAYS");
                case 4:
                    vaalListPass.Add(userName);
                    return $"Type !vaal_pass <user> to select a willing user to pass the vaal to. Other users type !vaal_willing if you are willing. You have 1 minute.";
                case 5:
                    var subscriptions = botApi.Helix.Subscriptions.CheckUserSubscriptionAsync(channelId, userId).Result.Data; // Is this not working?
                    if (!subscriptions.Any())
                    {
                        logger.Log(userName, "SubOwed_Self", -1);
                        return $"They will be gifted a sub by {channel} after stream ends unless they are already subbed by then.";
                    }
                    vaalListGift.Add(userName);
                    return $"Type !vaal_gift <user> to select a user to sub. You have 1 minute.";
                case 6:
                    vaalListTimeout.Add(userName);
                    return $"Type !vaal_timeout <user> to select a user to timeout for 1 hour. You have 1 minute.";
                default:
                    throw new InvalidOperationException("UNEXCPTED VAAL RESULT VALUE");
            }
        }

        private void SendVaalMessage(string rollUser, string source)
        {
            var diceRoll = new Random().Next(6) + 1;
            SendVaalMessageWithRoll(rollUser, source, diceRoll);
        }

        private void SendVaalMessageWithRoll(string rollUser, string source, int diceRoll)
        {
            var userId = GetUser(rollUser)?.Id;
            if (userId is null)
            {
                throw new InvalidOperationException($"COULD NOT GET ID FOR {rollUser}");
            }
            var isMod = streamerApi.Helix.Moderation.GetModeratorsAsync(channelId, new[] { rollUser }.ToList()).Result.Data.Any();
            if (userId == channelId || isMod)
            {
                throw new InvalidOperationException($"CANNOT VAAL MOD {rollUser}");
            }
            logger.Log(rollUser, source, diceRoll);

            context.Clients.Group(VaalHub.VaalGroupId).SendAsync("ReceiveVaalResult", rollUser, diceRoll).Wait();
            Thread.Sleep(TimeSpan.FromSeconds(5)); // Increase this later so that the on-screen vaal shows first.
            var chatMessage = $"{rollUser} vaaled themselves and rolled a {diceRoll}! ";
            chatMessage += HandleVaalResult(diceRoll, rollUser, userId);
            client.SendMessage(channel, chatMessage);
        }

        private void Client_OnLog(object? _, TwitchLib.Client.Events.OnLogArgs e)
        {
            Console.WriteLine($"{e.DateTime.ToString()}: {e.BotUsername} - {e.Data}");
        }

        private void OnConnected(object? _, OnConnectedArgs e)
        {
            Console.WriteLine($"Connected to {e.AutoJoinChannel}");
        }

        private void OnMessageReceived(object? _, OnMessageReceivedArgs e)
        {
            var messageContent = e.ChatMessage.Message;
            if (!messageContent.StartsWith("!vaal"))
            {
                return;
            }

            var messageUser = e.ChatMessage.DisplayName;
            if (messageContent.StartsWith("!vaal_willing"))
            {
                vaalListWilling.Add(messageUser);
                return;
            }

            var splitString = messageContent.Split();
            if (splitString.Length < 2)
            {
                client.SendMessage(channel, $"@{messageUser} error no user specified.");
                return;
            }
            var targetUser = splitString[1];
            if (messageContent.StartsWith("!vaal_pass") && vaalListPass.Contains(messageUser))
            {
                if (GetUser(targetUser) is null)
                {
                    client.SendMessage(channel, $"@{messageUser} error user {targetUser} does not exist.");
                    return;
                }
                if (vaalListWilling.Contains(targetUser))
                {
                    if (vaalListPass.Remove(messageUser))
                    {
                        SendVaalMessage(targetUser, PassedSource);
                    }
                    else
                    {
                        client.SendMessage(channel, $"@{messageUser} error race condition.");
                    }
                }
                else
                {
                    client.SendMessage(channel, $"@{messageUser} error user {targetUser} is not willing.");
                }
                return;
            }

            if (messageContent.StartsWith("!vaal_gift") && vaalListGift.Contains(messageUser))
            {
                if (GetUser(targetUser) is null)
                {
                    client.SendMessage(channel, $"@{messageUser} error user {targetUser} does not exist.");
                    return;
                }
                vaalListGift.Remove(messageUser);
                logger.Log(targetUser, "SubOwed_Gift", -1, $"From {messageUser}");
                client.SendMessage(channel, $"User {targetUser} will be gifted a sub by {channel} after stream ends unless they are already subbed by then.");
                return;
            }

            if (messageContent.StartsWith("!vaal_timeout") && vaalListTimeout.Contains(messageUser))
            {
                var getUser = GetUser(targetUser);
                if (getUser is null)
                {
                    client.SendMessage(channel, $"@{messageUser} error user {targetUser} does not exist.");
                    return;
                }
                vaalListTimeout.Remove(messageUser);
                try
                {
                    var banUserRequest = new BanUserRequest();
                    banUserRequest.UserId = getUser.Id;
                    banUserRequest.Duration = 3600; // 1 hour
                    banUserRequest.Reason = $"You were sacrificed by {messageUser}";
                    var banResult = moderationWithBanUser.BanUserAsync(channelId, botUserId, banUserRequest).Result;
                    logger.Log(targetUser, "Timeout", -1, $"From {messageUser}");
                    client.SendMessage(channel, $"User {targetUser} has been sacrificed by {messageUser}.");
                }
                catch
                {
                    logger.Log(targetUser, "TimeoutFail", -1, $"From {messageUser}");
                    client.SendMessage(channel, $"User {targetUser} could not be sacrificed.");
                }
                return;
            }

            if (e.ChatMessage.IsBroadcaster || e.ChatMessage.IsModerator)
            {
                if (messageContent.StartsWith("!vaal_override", StringComparison.OrdinalIgnoreCase))
                {
                    if (GetUser(targetUser) is null)
                    {
                        client.SendMessage(channel, $"@{messageUser} error user {targetUser} does not exist.");
                        return;
                    }
                    if (e.ChatMessage.IsBroadcaster
                        && splitString.Length >= 3
                        && int.TryParse(splitString[2], out var forcedResult)
                        && forcedResult <= 6
                        && forcedResult >= 1)
                    {
                        SendVaalMessageWithRoll(targetUser, OverrideSource + $"{forcedResult}_" + messageUser, forcedResult);
                    }
                    else
                    {
                        SendVaalMessage(targetUser, OverrideSource + "_" + messageUser);
                    }
                }
            }
        }

        private void OnWhisperReceived(object? _, OnWhisperReceivedArgs e)
        {
            if (e.WhisperMessage.Username == "my_friend")
                client.SendWhisper(e.WhisperMessage.Username, "Hey! Whispers are so cool!!");
        }

        private TwitchLib.Api.Helix.Models.Users.GetUsers.User? GetUser(string userName) => botApi.Helix.Users.GetUsersAsync(logins: new[] { userName }.ToList()).Result.Users.FirstOrDefault();
    }
}
