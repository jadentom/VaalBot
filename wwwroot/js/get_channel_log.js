﻿"use strict";

const uri = 'api/Rewards?broadcaster=';

function getLogForChannel() {
    const channelTextElement = document.getElementById('channelText');
    const channelLogContentElement = document.getElementById("channelLogContent");
    fetch(uri + channelTextElement.value.trim())
        .then(response => response.text())
        .then(text => channelLogContentElement.innerText = text);
}
