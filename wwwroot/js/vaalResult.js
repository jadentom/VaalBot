﻿"use strict";

var myTimeout = null;
var connection = new signalR.HubConnectionBuilder().withUrl("/vaalHub").build();

// http://dougtesting.net/winwheel/docs/tut14_setting_the_prize
let theWheel = new Winwheel({
    'numSegments': 6,
    'outerRadius': 170,
    'segments':
        [
            { 'fillStyle': '#eae56f', 'text': '1' },
            { 'fillStyle': '#89f26e', 'text': '2' },
            { 'fillStyle': '#7de6ef', 'text': '3' },
            { 'fillStyle': '#e7706f', 'text': '4' },
            { 'fillStyle': '#eae56f', 'text': '5' },
            { 'fillStyle': '#89f26e', 'text': '6' }
        ],
    'animation':
    {
        'type': 'spinToStop',
        'duration': 5,
        'spins': 8,
        'callbackAfter': 'drawTriangle()'
    }
});

function drawTriangle() {
    // Get the canvas context the wheel uses.
    let ctx = theWheel.ctx;

    ctx.strokeStyle = 'navy';  // Set line colour.
    ctx.fillStyle = 'aqua';  // Set fill colour.
    ctx.lineWidth = 2;
    ctx.beginPath();           // Begin path.
    ctx.moveTo(170, 5);        // Move to initial position.
    ctx.lineTo(230, 5);        // Draw lines to make the shape.
    ctx.lineTo(200, 40);
    ctx.lineTo(171, 5);
    ctx.stroke();              // Complete the path by stroking (draw lines).
    ctx.fill();                // Then fill.
}

connection.start().then(function () {
    // Subscribe to vaal results
    connection.invoke("JoinVaalGroup").catch(function (err) {
        return console.error(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
});

connection.on("ReceiveVaalResult", function (user, result) {
    if (myTimeout !== null) {
        clearTimeout(myTimeout);
        myTimeout = null;
    }

    var userDiv = document.getElementById("userDiv");
    userDiv.style.visibility = 'visible';
    userDiv.textContent = "Vaaling:" + user;
    var canvas = document.getElementById("canvas");
    canvas.style.visibility = 'visible';
    var audio = new Audio('/resources/VaalVaalVaal.mp3');
    audio.play();

    // Get random angle inside specified segment of the wheel.
    let stopAt = theWheel.getRandomForSegment(result);

    // Reset wheel
    theWheel.stopAnimation(false);
    theWheel.rotationAngle = 0;
    theWheel.draw();

    // Important thing is to set the stopAngle of the animation before stating the spin.
    theWheel.animation.stopAngle = stopAt;

    // Start the spin animation here.
    try {
        theWheel.startAnimation();
    }
    catch (err) {
        console.error(err.toString())
    }

    myTimeout = setTimeout(function () {
        userDiv.style.visibility = 'hidden';
        canvas.style.visibility = 'hidden';
    }, 10000);
});