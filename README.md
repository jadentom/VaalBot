# Quickstart

- Create a 6 line file in the home directory for bot credentials:
    1. UserName
    2. Bot client ID - chat:edit, chat:read, user:read:subscriptions, moderator:manage:banned_users on https://twitchtokengenerator.com/ for bot
    3. Bot access token - access token for above
    4. Name of channel to connect to
    5. Streamer client ID - channel:read:redemptions and moderation:read on https://twitchtokengenerator.com for STREAMER
    6. Streamer access token - access token for above
- `dotnet build`
- `dotnet /my/path/to/VaalBot.dll --urls "http://*:5000;https://*:5001"`
- You can now navigate to `http://MyHost:5000/VaalWheel`

# Misc

We're have a submodule because only dev branch of TwitchLib.Api has the helix ban.