﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VaalBot.Pages
{
    public class VaalWheelModel : PageModel
    {
        private readonly ILogger<VaalWheelModel> _logger;

        public VaalWheelModel(ILogger<VaalWheelModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {

        }
    }
}
